# ClearBank_API_Using_JMeter

## Decision Factor
- As I have mentioned that I have used JMeter for API Automation, I created automation suite using JMeter.
- Using JMeter, a non-technical QA can also automate APIs
- Also, I have added ant within JMeter, so that ant need not to be installed in the system.
- Created .bat file so that anyone can just double click the file and run the complete automation

## Instructions
- Install Java JDK and set JAVA_HOME path in system environment variable
- Now add %JAVA_HOME%\bin in Path system environment variable
- Clone the repository
- Go to JMeter/extras and run ClearBank.bat file
- HTML Result will be generated in extras folder with name - ClearBank_YYYYMMDD_HHMM.html